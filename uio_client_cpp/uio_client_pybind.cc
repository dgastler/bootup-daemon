/*
 * Python binding for the UIOClient C++ object.
 */

#include "uio_client.hh"

#include "pybind11/pybind11.h"
#include "pybind11/stl.h"

namespace py = pybind11;

/*
 * The methods of the UIOClient that are ported to Python are defined below.
 * Note that the function pointer casting is required to treat the overloaded methods.
 */
PYBIND11_MODULE(UIOClient, m) {
    /* Register the UIOClient class and its methods. */
    py::class_<UIOClient>(m, "UIOClient")
        .def(py::init<std::string>())
        .def("Init",      static_cast<UIOStatus (UIOClient::*)()>(&UIOClient::Init), "Initiate the client: Obtains a client ID from the server.")
        .def("Init",      static_cast<UIOStatus (UIOClient::*)(std::string)>(&UIOClient::Init), "Initiate the client with an ID passed in as an argument.")
        .def("SendLoadTarMessage",       &UIOClient::SendLoadTarMessage,      "Send a message to load a UIO tarball.")
        .def("SendUnloadTarMessage",     &UIOClient::SendUnloadTarMessage,    "Send a message to unload a UIO tarball.")
        .def("SendCMPowerUpMessage",     &UIOClient::SendCMPowerUpMessage,    "Send a message to power up the Command Module.")
        .def("SendProgramFPGAMessage",   &UIOClient::SendProgramFPGAMessage,  "Send a message to program the FPGA.")
        .def("GetUIODevices",     static_cast<UIOStatus (UIOClient::*)(py::dict&)>(&UIOClient::GetUIODevices),   "Get a list of UIO devices installed and who installed them.")
        .def("GetUIDList",        static_cast<UIOStatus (UIOClient::*)(py::list&)>(&UIOClient::GetUIDList),      "Get a list of client IDs known by the server.")
        .def("GetUID",            &UIOClient::GetUID,            "Get the client ID of this UIOClient instance.")
        .def("GetServerResponse", &UIOClient::GetServerResponse, "Get the server response for the latest transaction with this client.");

    /* Register the UIOStatus enum. */
    py::enum_<UIOStatus>(m, "UIOStatus")
        .value("OK",                  UIOStatus::OK)
        .value("FILE_NOT_FOUND",      UIOStatus::FILE_NOT_FOUND)
        .value("UID_NOT_SET",         UIOStatus::UID_NOT_SET)
        .value("SOCK_WRITE_FAILED",   UIOStatus::SOCK_WRITE_FAILED)
        .value("SOCK_READ_FAILED",    UIOStatus::SOCK_READ_FAILED)
        .value("SERVER_ERROR",        UIOStatus::SERVER_ERROR)
        .value("XML_PARSER_ERROR",    UIOStatus::XML_PARSER_ERROR)
        .export_values();
}