import logging

from .definitions import DEVICE_TREE_UUID

logger = logging.getLogger(__name__)

def get_node_ids_to_remove_for_uuid(uuid, loaded_nodes):
    """
    Get the list of node IDs to remove, corresponding to the given UUID.  
    """
    node_ids_to_remove = []
    for node_id, node in loaded_nodes.items():
        # If UUID is "all", we want to remove all the nodes that are not loaded
        # automatically, e.g. uuid="device-tree".
        if uuid == "all-users" and node["uuid"] != DEVICE_TREE_UUID:
            node_ids_to_remove.append(node_id)
    
        # If UUID is a particular client ID, we just want to remove the 
        # corresponding nodes for that client. This is orthogonal to the if case above.
        elif uuid != "all-users" and node["uuid"] == uuid:
            node_ids_to_remove.append(node_id)
    
    return node_ids_to_remove


def update_loaded_nodes_for_cleanup(uuid, loaded_nodes,baMap):
    """
    For the given client ID (uuid), update the loaded_nodes dictionary by removing
    the UIO devices (nodes) that are loaded by this client ID.
    """
    # Based on the given UUID, figure out which nodes we want to remove
    node_ids_to_remove = get_node_ids_to_remove_for_uuid(uuid, loaded_nodes)

    for node_id in node_ids_to_remove:
        node = loaded_nodes[node_id]

        # Sanity check: We should not be attempting to clean up UIO devices
        # that are auto-loaded on system boot. 
        if node["uuid"] == DEVICE_TREE_UUID:
            continue

        # remove this address from the baMap
        baMap.FreeBlock(int(str(node["address"]),0))
        
        # Remove this node from the set of loaded nodes
        loaded_nodes.pop(node_id)
