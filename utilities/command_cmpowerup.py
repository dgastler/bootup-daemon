import logging

from .enums import CMPowerUpStatus
from .helpers_uuid import get_uuid_from_message, uuid_is_authenticated
from .helpers_connection import send_message

logger = logging.getLogger(__name__)

def power_up_cm_for_uuid(
  incoming_message,
  cmpwrup_timeout,
  sm_instance,
):
  """
  Power up the Command Module. Returns an enum representing the status of the operation.
  """
  # Get UUID from message
  try:
    uuid = get_uuid_from_message(incoming_message)

  # Invalid message given to the server
  except IndexError:
    return CMPowerUpStatus.MSG_INVALID

  if not uuid_is_authenticated(uuid):
    return CMPowerUpStatus.AUTH_FAILED

  # Power up the CM
  status = sm_instance.PowerUpCM(1, cmpwrup_timeout)

  # status=False, meaning PowerUpCM call failed
  if not status:
    logger.error(f"PowerUpCM(1, {cmpwrup_timeout}) call returned False, could not power up CM.")
    return CMPowerUpStatus.CMPWRUP_FAILED

  return CMPowerUpStatus.OK

def power_up_cm_and_send_msg_back_to_client(
    connection,
    incoming_message,
    outgoing_message,
    cmpwrup_timeout,
    sm_instance,
  ):
  """
  Power up the Command Module, and send a message back to client.
  """
  status = power_up_cm_for_uuid(incoming_message, cmpwrup_timeout, sm_instance)

  if status == CMPowerUpStatus.OK:
    outgoing_message.strdata = "Powered up the CM."
  
  elif status == CMPowerUpStatus.MSG_INVALID:
    outgoing_message.strdata = "Invalid message, UUID not specified."

  elif status == CMPowerUpStatus.AUTH_FAILED:
    outgoing_message.strdata = "Not authenticated to power up the CM, please send a HELLO message first."

  elif status == CMPowerUpStatus.CMPWRUP_FAILED:
    outgoing_message.strdata = "Failed to power up the CM, PowerUpCM() call returned False."

  # Send the message back to client
  send_message(outgoing_message, connection)