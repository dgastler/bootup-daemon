import os
import logging
import subprocess
import shutil

from .definitions import CLEANUP_QUEUES, LOADED_UIOS_PER_UID
from .enums import FWLoadStatus

from .helpers_uuid import uuid_is_authenticated, get_uuid_from_message
from .helpers_address_table import write_address_table, write_connections_file
from .helpers_connection import send_message
from .helpers_overlay import ProcessFWDir
from .helpers_cleanup import cleanup, removeObject

logger = logging.getLogger(__name__)


def save_and_extract_fw_tarball(fw_dir, incoming_message, uuid):
  """
  From the incoming message, read the tarball content and save it under a new tarball file,
  under the given tarball target path. Finally, extract that new tarball under the same directory.

  If the tar command fails to extract the tarball, automatically clean up the directory 
  created for the client under /fw. 
  """
  # If this output directory exists, we won't try to override
  if os.path.exists(fw_dir):
    return FWLoadStatus.DIR_EXISTS

  # If the directory is new, proceed
  os.makedirs(fw_dir)

  logger.info(f"FW directory created: {fw_dir}")

  # 1) Save the FW tarball that the server received under this output directory.
  outfile = os.path.join(fw_dir, "firmware.tar.gz")
  with open(outfile, "wb") as f:
    f.write(incoming_message.bytesdata)

  # 2) Now, extract this new tarball
  proc = subprocess.Popen(["tar", "-xf", outfile, "-C", fw_dir], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  stdout, stderr = proc.communicate()

  # tar command failed
  # If the extraction of the tarball fails, automatically clean up
  if proc.returncode != 0:
    logger.error(f"Extraction failed for tarball: {outfile}")
    cleanup(uuid)
    
    logger.info(f"Removing client {uuid}")
    LOADED_UIOS_PER_UID.pop(uuid)
    
    return FWLoadStatus.TAR_FAILED

  # If extraction of the tarball succeeded, all good.
  logger.info(f"Tarball extracted under {fw_dir}")

  return FWLoadStatus.OK


def load_fw_for_uuid(
    incoming_message,
    loaded_nodes: dict,
    baMap,
    address_table_path: str,
    tarball_target_path: str
  ) -> FWLoadStatus:
  """
  Given the incoming message object, do the following:

  - Check if this UUID is authenticated to load firmware
  - Read the contents of the firmware file and save it under a tarball under the specified target path
  - Load the UIO devices as specified in this firmware.
  """
  # Figure out the UUID of client from the client message
  try:
    uuid = get_uuid_from_message(incoming_message)
  except IndexError:
    return FWLoadStatus.MSG_INVALID

  # Check auth
  if not uuid_is_authenticated(uuid):
    return FWLoadStatus.NO_AUTH

  # 1) Extract the input tarball file and save it under a client-specific FW directory.
  fw_dir = os.path.join(tarball_target_path, uuid)
  # Add to cleanup
  CLEANUP_QUEUES[uuid].put(fw_dir)

  status = save_and_extract_fw_tarball(fw_dir, incoming_message, uuid)

  # If the processing of the tarball fails, exit from the function and return the failure status.
  if status != FWLoadStatus.OK:
    return status

  try:
    # 2) Process the firmware directory, load the UIO devices
    loaded_nodes.update(ProcessFWDir(fw_dir, address_table_path, baMap, uuid))
    # 3) Remove the connections.xml file and the address_apollo.xml file (in order).
    # We'll rewrite them in the next step with the new list of nodes we have.
    removeObject(os.path.join(address_table_path, "connections.xml"))
    removeObject(os.path.join(address_table_path, "address_apollo.xml"))
    # 4) Write the final address table and the connections file
    write_address_table(os.path.join(address_table_path, "address_apollo.xml"), loaded_nodes, baMap)
    write_connections_file(address_table_path, "connections.xml")
  # Handle the case where FW loading failed for this directory, 
  # clean up any file that is related to this client ID
  # and return an error state.
  except BaseException:
    cleanup(uuid)
    # Remove this client ID from the list of active IDs
    LOADED_UIOS_PER_UID.pop(uuid)
    return FWLoadStatus.FW_LOAD_FAILED

  # All OK if we reach this state
  return FWLoadStatus.OK


def load_fw_for_uuid_and_send_msg_to_client(
    connection,
    incoming_message,
    outgoing_message,
    loaded_nodes,
    baMap,
    address_table_path,
    tarball_target_path
):
  """
  Load FW for this UUID and send a proper reply back to the client.
  """
  status = load_fw_for_uuid(incoming_message, loaded_nodes, baMap, address_table_path, tarball_target_path)

  if status == FWLoadStatus.OK:
    outgoing_message.strdata = "CM FW is successfully loaded."
  
  elif status == FWLoadStatus.MSG_INVALID:
    outgoing_message.strdata = "Invalid message, client ID is not specified."

  elif status == FWLoadStatus.NO_AUTH:
    outgoing_message.strdata = "Not authenticated to load FW, please obtain a client ID first."

  elif status == FWLoadStatus.DIR_EXISTS:    
    outgoing_message.strdata = "This client has already loaded firmware, please unload that first."

  elif status == FWLoadStatus.TAR_FAILED:
    outgoing_message.strdata = "tar command returned non-zero exit status, untarring failed."

  elif status == FWLoadStatus.FW_LOAD_FAILED:
    outgoing_message.strdata = "Failed to process FW directory and load UIO devices."

  # Send the message back to client
  send_message(outgoing_message, connection)
