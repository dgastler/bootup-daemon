# 
# Functions related to address table IO.
# 

import os
import logging
import xml.etree.ElementTree
import xml.dom.minidom as minidom

logger = logging.getLogger(__name__)

def write_address_table(address_table_path, loaded_nodes, baMap):
  """
  Given the list of loaded nodes, update the XML address table under the given path.
  """
  root = xml.etree.ElementTree.Element("node")
  root.attrib['id'] = "Top"
  root.text="\n\t"
  
  # Check loaded_nodes for succesful loads - None in this dict already had a UIO file
  for node_id in loaded_nodes.keys():
    if "error" == node_id:
      new_node = xml.etree.ElementTree.Comment(loaded_nodes["error"])
      new_node.tail="\n\t"
      root.insert(1,new_node)
    else:
      logger.debug(f'Merged address table: Adding {node_id}')
      new_node = xml.etree.ElementTree.SubElement(root, 'node')
      new_node.attrib['id'] = loaded_nodes[node_id]['id']
#      new_node.attrib['address'] = "0x"+hex(loaded_nodes[node_id]['address'])[2:].zfill(8)
      new_node.attrib['address'] = str(loaded_nodes[node_id]['address'])
      if not (loaded_nodes[node_id].get('module') is None):
        new_node.attrib['module'] = loaded_nodes[node_id]['module']
      if not (loaded_nodes[node_id].get('fwinfo') is None):
        new_node.attrib['fwinfo'] = loaded_nodes[node_id]['fwinfo']
      new_node.tail = "\n\t"
  new_node.tail="\n"

  # Write output to address table
  xml.etree.ElementTree.ElementTree(root).write(address_table_path)

def write_connections_file(address_table_path, file_name="connections.xml"):
  """
  Creates the address table path (if it doesn't exist already), and generates+saves the
  connections.xml file to this directory.
  """
  if not os.path.exists(address_table_path):
    logger.info(f"Creating directory {address_table_path}")
    os.makedirs(address_table_path)
  
  # Now, generate the connections file.
  output_path = os.path.join(address_table_path, file_name)
  # Create the root element
  root = xml.etree.ElementTree.Element("connections")

  # Create the connection element
  connection = xml.etree.ElementTree.SubElement(root, "connection")
  connection.set("id", "test.0")
  connection.set("uri", "uioaxi-1.0:///opt/address_table/address_apollo.xml")
  connection.set("address_table", "file:///opt/address_table/address_apollo.xml")

  # Create the comment element
  comment = xml.etree.ElementTree.Comment("be sure to use the same file in both 'uri' and 'address_table'")
  root.insert(0, comment)

  # Create a string representation of the XML
  xml_string = xml.etree.ElementTree.tostring(root, encoding="utf-8")

  # Create a prettified version of the XML string. This is necessary for proper indentation
  dom = minidom.parseString(xml_string)
  pretty_xml_string = dom.toprettyxml(indent="  ", encoding="utf-8")

  # Write the prettified XML to a file
  with open(output_path, "wb") as file:
    file.write(pretty_xml_string)
