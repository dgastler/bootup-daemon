default: build

UIO_CLIENT_CPP_PATH=./uio_client_cpp
PROTOBUF_SRC_PATH=./protobuf_src

# Update submodules
init:
	git submodule update --init --recursive

# Build UIOClient software
build:
	$(MAKE) -C ${PROTOBUF_SRC_PATH} build
	$(MAKE) -C ${UIO_CLIENT_CPP_PATH} build

clean:
	$(MAKE) -C ${PROTOBUF_SRC_PATH} clean
	$(MAKE) -C ${UIO_CLIENT_CPP_PATH} clean

install:
	$(MAKE) -C ${UIO_CLIENT_CPP_PATH} install